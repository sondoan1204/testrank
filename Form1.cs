﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestRank
{
    public partial class Form1 : Form
    {
        public class ListTextRank: IComparable<ListTextRank>
        {
            public string databegin { get; internal set; }
            public string datafilter { get; internal set; }
            public int id { get; internal set; }
            public int linkOut { get; internal set; }
            public List<int> linkIn { internal get; set; }
            public float PageRankStart { get; internal set; }
            public float PageRankNext { get; internal set; }
            public int CompareTo(ListTextRank other)
            {
                return this.PageRankStart.CompareTo(other.PageRankStart);
            }
        }
        public class Reslemmatizer
        {
            public string word { get; set; }
        }
        public class Resstop_words
        {
            public string[] filtered_sentence { get; set; }
            public string[] word_tokens { get; set; }
        }
        public class ResWordnetLemmatizer
        {
            public string filtered_sentence { get; set; }
            public string word_tokens { get; set; }
        }
        List<ListTextRank> _list = new List<ListTextRank>();
        List<float> PageRankTam = new List<float>();
        int SoTu = 0;
        public class SortPageRank : IComparer 
        {
            int IComparer.Compare(object x, object y)
            {
                ListTextRank ls1 = x as ListTextRank;
                ListTextRank ls2 = y as ListTextRank;
                return ls1.PageRankStart.CompareTo(ls2.PageRankStart);
                //if (ls1.PageRankStart > ls2.PageRankStart)
                //    return 1;
                //if (ls1.PageRankStart < ls2.PageRankStart)
                //    return -1;
                //return 0;
            }
        }

        public Form1()
        {
            InitializeComponent();
            numericUpDownSoLuong.Value = 4;
            listViewLoad.View = View.Details;
            listViewLoad.GridLines = true;
            listViewLoad.FullRowSelect = true;
        }

        private async void ButtonLoadFile_Click(object sender, EventArgs e)
        {
            SoTu= Int32.Parse(numericUpDownSoLuong.Text);
            using (OpenFileDialog ofd = new OpenFileDialog() { Multiselect = false })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    using (StreamReader rd = new StreamReader(ofd.FileName))
                    {
                        //richTextBoxLoadData.Text = await rd.ReadToEndAsync();

                        var items = new List<string>();
                        int index = 0;
                        foreach (Match match in Regex.Matches(await rd.ReadToEndAsync(), "<s [^>]*>(.*?)</s>"))
                        {
                            items.Add(match.Groups[1].Value);
                            List<int> firstlist = new List<int>();
                            _list.Add(new ListTextRank { databegin = match.Groups[1].Value, PageRankStart = 1, id=index,linkOut=0, linkIn = firstlist });
                            index++;
                        }
                        //string output = String.Join(" ", items);
                        //richTextBoxLoadData.Text = String.Join(Environment.NewLine, items);
                        
                       

                        Cursor = Cursors.WaitCursor; // change cursor to hourglass type
                        //string parameter = "Hello world!";
                        //Thread t = new Thread(new ParameterizedThreadStart(XuLyCauL3(_list)));
                        //t.Start(parameter);
                        XuLyCauL3(_list);
                        XuLyCauL2(_list);
                        TinhBacCau(_list);

                        //Load listView
                        listViewLoad.Columns.Add("STT");
                        listViewLoad.Columns.Add("NoiDung");
                        listViewLoad.Columns.Add("Data");
                        foreach (ListTextRank value in _list)
                        {
                            listViewLoad.Items.Add(_list.IndexOf(value).ToString());
                            listViewLoad.Items[_list.IndexOf(value)].SubItems.Add(value.databegin);
                            listViewLoad.Items[_list.IndexOf(value)].SubItems.Add(value.datafilter);
                        }
                        listViewLoad.BeginUpdate();
                        listViewLoad.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                        listViewLoad.EndUpdate();
                        listViewLoad.Refresh();

                        //Lap tính PR
                        for (int i = 0; i <= 1000; i++)
                        {
                            TinhPageRank(_list);
                            UpdatePageRankS(_list);
                        }

                        ////Tính PR 1 lần
                        //TinhPageRank(_list);
                        //UpdatePageRankS(_list);

                        //foreach (ListTextRank value in _list)
                        //{
                        //    //richTextBoxOutPut.Text = value.datafilter + Environment.NewLine;
                        //    richTextBoxOutPut.AppendText(_list.IndexOf(value) + value.datafilter + Environment.NewLine);
                        //}

                        //In ma tran
                        for (int i = 0 ; i < _list.Count; i++)
                        {
                            richTextBoxDoThi.AppendText("S" + i+ ": ");
                            for (int j=0; j < _list.Count; j++)
                            {
                                if (_list[i].linkIn.Contains(j) == true)
                                    richTextBoxDoThi.AppendText(" 1 ");
                                else
                                    richTextBoxDoThi.AppendText(" 0 ");
                            }
                            richTextBoxDoThi.AppendText(Environment.NewLine);
                        }


                        _list.Sort();
                        for (int i = _list.Count - 1; i >= 0; i--)
                        {
                            richTextBoxKetQua.AppendText(_list[i].databegin + ": " + _list[i].PageRankStart + Environment.NewLine);
                        }

                        //in tất cả PR sau khi tính
                        //foreach (ListTextRank value in _list)
                        //{
                        //    //richTextBoxOutPut.Text = value.datafilter + Environment.NewLine;
                        //    richTextBoxKetQua.AppendText(value.datafilter +": "+ value.PageRankStart + Environment.NewLine);
                        //}



                        Cursor = Cursors.Arrow; // change cursor to normal type
                    }
                }
            }
        }
        public void XuLyCauL1(List<ListTextRank> lists)
        {
            foreach (ListTextRank value in lists)
            {
                var srt = value.databegin.Replace("'", "").Replace("`", "").Split();
                //var srt = value.databegin.Replace(".", "").Replace(",", "").Replace("\"", "").Replace("'", "").Replace("`", "").Split();
                for (int i = 0; i < srt.Length; i++)
                {
                    srt[i] = ChuanHoaTu(srt[i]);
                }
                string result = string.Join(" ", srt);
                value.datafilter = result;
            }
        }
        public void XuLyCauL3(List<ListTextRank> lists)
        {
            foreach (ListTextRank value in lists)
            {
                var srt = value.databegin.Replace("'", "").Replace("`", "").Replace("\"", "");
                value.datafilter = WordnetLemmatizer(srt).Replace(".", "").Replace(",", "");
            }
        }
        public void XuLyCauL2(List<ListTextRank> lists)
        {
            foreach (ListTextRank value in lists)
            {
                var result = XoaStopWords(value.datafilter);
                value.datafilter = result;
            }
        }
        public void TinhBacCau(List<ListTextRank> lists)
        {
            for(int i=0; i < lists.Count; i++)
            {
                for(int j=0;j< lists.Count; j++)
                {
                    if (lists[i].id == lists[j].id)
                        continue;
                    if (SoSanhChuoi(lists[i].datafilter, lists[j].datafilter) >= SoTu)
                    {
                        lists[i].linkOut = lists[i].linkOut + 1;
                        lists[i].linkIn.Add(lists[j].id);
                    }
                }
            }
        }
        public string ChuanHoaTu(string srt)
        {
            try
            {
                string webAddr = "http://127.0.0.1:5000/lemmatizer";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"word\":\"" + srt + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    Console.WriteLine(responseText);
                    var reps = JsonConvert.DeserializeObject<Reslemmatizer>(responseText);
                    //Console.WriteLine(reps.word);
                    return reps.word;
                }
            }
            catch
            {
                return srt;
            }

        }
        public string XoaStopWords(string srt)
        {
            //try
            //{
                string webAddr = "http://127.0.0.1:5000/stop_words";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{\"sentence\":\"" + srt + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    Console.WriteLine(responseText);
                    var reps = JsonConvert.DeserializeObject<Resstop_words>(responseText);
                    //Console.WriteLine(reps.word);
                    return string.Join(" ", reps.filtered_sentence);
                }
            //}
            //catch
            //{
            //    return srt;
            //}

        }
        public string WordnetLemmatizer(string srt)
        {
            //try
            //{
            string webAddr = "http://127.0.0.1:5000/WordnetLemmatizer";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"sentence\":\"" + srt + "\"}";
                streamWriter.Write(json);
                streamWriter.Flush();
            }
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                Console.WriteLine(responseText);
                var reps = JsonConvert.DeserializeObject<ResWordnetLemmatizer>(responseText);
                //Console.WriteLine(reps.word);
                return string.Join(" ", reps.filtered_sentence);
            }
            //}
            //catch
            //{
            //    return srt;
            //}

        }
        public int SoSanhChuoi(string srt1, string srt2)
        {
            //test  so sánh chuỗi
            var res = srt1.Split().Intersect(srt2.Split());
            return res.Count();
        }

        public float TongZM(List<int> linkIn)
        {
            float tong = 0;
            foreach (int i in linkIn)
            {
                tong = tong + (_list[i].PageRankStart / (float)_list[i].linkOut);
            }
            return tong;
        }
        //public void TinhPageRank(List<ListTextRank> lists)
        //{
        //     for(int i=0; i < lists.Count; i++)
        //    {
        //        _list[i].PageRankNext = (((float)1 - (float)0.85) / (float)lists.Count()) + (float)0.85 * TongZM(_list[i].linkIn);
        //        PageRankTam.Add(_list[i].PageRankNext);
        //    }
        //}
        //bỏ d
        public void TinhPageRank(List<ListTextRank> lists)
        {
            for (int i = 0; i < lists.Count; i++)
            {
                _list[i].PageRankNext =  TongZM(_list[i].linkIn);
                PageRankTam.Add(_list[i].PageRankNext);
            }
        }

        public void UpdatePageRankS(List<ListTextRank> lists)
        {
            for (int i = 0; i < lists.Count; i++)
            {
                _list[i].PageRankStart = PageRankTam[i];
            }
        }

        private void ListViewLoad_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ListViewItem items in listViewLoad.SelectedItems)
            {
                richTextBoxData2.Text = items.SubItems[2].Text;
            }
        }

        //private void SetLoading(bool displayLoader)
        //{
        //    if (displayLoader)
        //    {
        //        this.Invoke((MethodInvoker)delegate
        //        {
        //            picLoader.Visible = true;
        //            this.Cursor = Cursors.WaitCursor;
        //        });
        //    }
        //    else
        //    {
        //        this.Invoke((MethodInvoker)delegate
        //        {
        //            picLoader.Visible = false;
        //            this.Cursor = Cursors.Default;
        //        });
        //    }
        //}
    }
}
