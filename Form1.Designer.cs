﻿namespace TestRank
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLoadFile = new System.Windows.Forms.Button();
            this.richTextBoxKetQua = new System.Windows.Forms.RichTextBox();
            this.numericUpDownSoLuong = new System.Windows.Forms.NumericUpDown();
            this.richTextBoxDoThi = new System.Windows.Forms.RichTextBox();
            this.listViewLoad = new System.Windows.Forms.ListView();
            this.richTextBoxData2 = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSoLuong)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLoadFile
            // 
            this.buttonLoadFile.Location = new System.Drawing.Point(69, 9);
            this.buttonLoadFile.Name = "buttonLoadFile";
            this.buttonLoadFile.Size = new System.Drawing.Size(179, 27);
            this.buttonLoadFile.TabIndex = 1;
            this.buttonLoadFile.Text = "Load File";
            this.buttonLoadFile.UseVisualStyleBackColor = true;
            this.buttonLoadFile.Click += new System.EventHandler(this.ButtonLoadFile_Click);
            // 
            // richTextBoxKetQua
            // 
            this.richTextBoxKetQua.Location = new System.Drawing.Point(658, 63);
            this.richTextBoxKetQua.Name = "richTextBoxKetQua";
            this.richTextBoxKetQua.Size = new System.Drawing.Size(190, 289);
            this.richTextBoxKetQua.TabIndex = 4;
            this.richTextBoxKetQua.Text = "";
            // 
            // numericUpDownSoLuong
            // 
            this.numericUpDownSoLuong.Location = new System.Drawing.Point(319, 16);
            this.numericUpDownSoLuong.Name = "numericUpDownSoLuong";
            this.numericUpDownSoLuong.Size = new System.Drawing.Size(81, 20);
            this.numericUpDownSoLuong.TabIndex = 6;
            // 
            // richTextBoxDoThi
            // 
            this.richTextBoxDoThi.Location = new System.Drawing.Point(36, 381);
            this.richTextBoxDoThi.Name = "richTextBoxDoThi";
            this.richTextBoxDoThi.Size = new System.Drawing.Size(812, 150);
            this.richTextBoxDoThi.TabIndex = 7;
            this.richTextBoxDoThi.Text = "";
            // 
            // listViewLoad
            // 
            this.listViewLoad.HideSelection = false;
            this.listViewLoad.Location = new System.Drawing.Point(36, 63);
            this.listViewLoad.Name = "listViewLoad";
            this.listViewLoad.ShowItemToolTips = true;
            this.listViewLoad.Size = new System.Drawing.Size(616, 289);
            this.listViewLoad.TabIndex = 8;
            this.listViewLoad.UseCompatibleStateImageBehavior = false;
            this.listViewLoad.SelectedIndexChanged += new System.EventHandler(this.ListViewLoad_SelectedIndexChanged);
            // 
            // richTextBoxData2
            // 
            this.richTextBoxData2.Location = new System.Drawing.Point(423, 9);
            this.richTextBoxData2.Name = "richTextBoxData2";
            this.richTextBoxData2.Size = new System.Drawing.Size(425, 48);
            this.richTextBoxData2.TabIndex = 9;
            this.richTextBoxData2.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 613);
            this.Controls.Add(this.richTextBoxData2);
            this.Controls.Add(this.listViewLoad);
            this.Controls.Add(this.richTextBoxDoThi);
            this.Controls.Add(this.numericUpDownSoLuong);
            this.Controls.Add(this.richTextBoxKetQua);
            this.Controls.Add(this.buttonLoadFile);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSoLuong)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonLoadFile;
        private System.Windows.Forms.RichTextBox richTextBoxKetQua;
        private System.Windows.Forms.NumericUpDown numericUpDownSoLuong;
        private System.Windows.Forms.RichTextBox richTextBoxDoThi;
        private System.Windows.Forms.ListView listViewLoad;
        private System.Windows.Forms.RichTextBox richTextBoxData2;
    }
}

